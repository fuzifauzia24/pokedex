import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  private apiUrl = 'https://pokeapi.co/api/v2/pokemon?limit=151';

  constructor(private http: HttpClient) {}

  getPokemons(): Observable<any> {
    return this.http.get<any>(this.apiUrl).pipe(
      map(response => response.results)
    );
  }

  getPokemonDetails(name: string): Observable<any> {
    const url = `https://pokeapi.co/api/v2/pokemon/${name}`;
    return this.http.get<any>(url);
  }
}
