import { HttpClient } from '@angular/common/http';
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrl: './pokemon-detail.component.css'
})
export class PokemonDetailComponent {
  public useSvgImage: boolean = false;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<PokemonDetailComponent>,
    private http: HttpClient
  ) {
    this.fetchPokemonDetails();
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  getStatItemClass(typeName: string): string {

    const typeClassMap: any = {
      normal: 'pokemon-type-normal',
      fire: 'pokemon-type-fire',
      water: 'pokemon-type-water',
      electric: 'pokemon-type-electric',
      grass: 'pokemon-type-grass',
      ice: 'pokemon-type-ice',
      fighting: 'pokemon-type-fighting',
      poison: 'pokemon-type-poison',
      ground: 'pokemon-type-ground',
      flying: 'pokemon-type-flying',
      psychic: 'pokemon-type-psychic',
      bug: 'pokemon-type-bug',
      rock: 'pokemon-type-rock',
      ghost: 'pokemon-type-ghost',
      dragon: 'pokemon-type-dragon',
      dark: 'pokemon-type-dark',
      steel: 'pokemon-type-steel',
      fairy: 'pokemon-type-fairy'
    };

    return typeClassMap[typeName] || '';
  }

  fetchPokemonDetails(): void {
    const url = `https://pokeapi.co/api/v2/pokemon-species/${this.data.pokemon.name}`;
    this.http.get(url).subscribe((speciesData: any) => {
      this.data.pokemon.description = speciesData.flavor_text_entries.find(
        (entry: any) => entry.language.name === 'en'
      ).flavor_text;
    });
  }

  playPokemonSound(): void {
    const url = `https://pokeapi.co/api/v2/pokemon/${this.data.pokemon.name}`;
    this.http.get(url).subscribe((pokemonData: any) => {
      const soundUrl = pokemonData.cries.latest;
      this.playSoundFile(soundUrl);
    });
  }

  private playSoundFile(url: string): void {
    const audio = new Audio(url);
    audio.play();
  }

  toggleImageFormat(useSvg: boolean): void {
    this.useSvgImage = useSvg;
  }
}
