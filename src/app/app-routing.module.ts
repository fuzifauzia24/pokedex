import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListPokemonComponent } from './list-pokemon/list-pokemon.component';
import { AppComponent } from './app.component';

const routes: Routes = [
  { path: '', redirectTo: '/list-pokemon', pathMatch: 'full' },
  { path: 'list-pokemon', component: ListPokemonComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
