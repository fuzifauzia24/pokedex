import { Component, ViewChild } from '@angular/core';
import { PokemonService } from '../service/pokemon-service.service';
import { MatDialog } from '@angular/material/dialog';
import { PokemonDetailComponent } from '../pokemon-detail/pokemon-detail.component';
import { MatPaginator } from '@angular/material/paginator';
import { animate, style, transition, trigger } from '@angular/animations';

interface Pokemon {
  name: string;
  url: string;
}
@Component({
  selector: 'app-list-pokemon',
  templateUrl: './list-pokemon.component.html',
  styleUrl: './list-pokemon.component.css',
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('300ms ease-out', style({ opacity: 1 })),
      ]),
      transition(':leave', [animate('300ms ease-in', style({ opacity: 0 }))]),
    ]),
  ],
})
export class ListPokemonComponent {
  pokemons: any[] = [];
  paginatedPokemons: any[] = [];
  searchTerm: any = '';
  pageSize = 20;
  pageSizeOptions: number[] = [20, 30, 50, 100]; 

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private pokemonService: PokemonService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.pokemonService.getPokemons().subscribe((data: Pokemon[]) => {
      data.forEach((pokemon) => {
        this.pokemonService
          .getPokemonDetails(pokemon.name)
          .subscribe((details) => {
            this.pokemons.push(details);
            this.paginatePokemons();
          });
      });
    });
  }

  paginatePokemons() {
    this.paginatedPokemons = this.pokemons.slice(0, this.pageSize);
  }

  applyFilter() {
    const filteredPokemons = this.pokemons.filter((pokemon) =>
      pokemon.name.toLowerCase().includes(this.searchTerm.toLowerCase())
    );
    this.paginatedPokemons = filteredPokemons.slice(0, this.pageSize);
  }

  onPageChange(event: any) {
    const startIndex = event.pageIndex * event.pageSize;
    const endIndex = startIndex + event.pageSize;
    const filteredPokemons = this.pokemons.filter((pokemon) =>
      pokemon.name.toLowerCase().includes(this.searchTerm.toLowerCase())
    );
    this.paginatedPokemons = filteredPokemons.slice(startIndex, endIndex);
  }

  openPokemonDialog(pokemon: any): void {
    const dialogRef = this.dialog.open(PokemonDetailComponent, {
      width: '600px',
      data: { pokemon: pokemon },
    });
  }

  getTypeColor(pokemon: any): string {
    return this.hasType(pokemon, 'normal')
      ? '#A8A878'
      : this.hasType(pokemon, 'grass')
      ? '#78c850'
      : this.hasType(pokemon, 'fire')
      ? '#f08030'
      : this.hasType(pokemon, 'water')
      ? '#6890f0'
      : this.hasType(pokemon, 'electric')
      ? '#f8d030'
      : this.hasType(pokemon, 'poison')
      ? '#a040a0'
      : this.hasType(pokemon, 'ground')
      ? '#e0c068'
      : this.hasType(pokemon, 'flying')
      ? '#a890f0'
      : this.hasType(pokemon, 'bug')
      ? '#a8b820'
      : this.hasType(pokemon, 'rock')
      ? '#b8a038'
      : this.hasType(pokemon, 'ghost')
      ? '#705898'
      : this.hasType(pokemon, 'psychic')
      ? '#f85888'
      : 'inherit';
  }

  hasType(pokemon: any, typeName: string): boolean {
    return pokemon.types[0].type.name === typeName;
  }
}
